/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jadva2;

/**
 *
 * @author Ho Hang
 */
public class Jadva2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        // TODO code application logic here
        //list of sales figures for each sales person
        int[] sales = new int[]{391,1234,1676,1041,340,482,1384,1681,362,1155,827,154,830,694,1967};
        commission co = new commission();
        int[] numincat = new int[]{0,0,0,0,0,0,0,0,0};
        
        //get commission bracket for each sales person
        for (int i = 0; i < sales.length; i++)
        {
            if (co.calccommission(sales[i]) >= 1000){
                numincat[0]++;
            }
            else if (co.calccommission(sales[i]) >= 900 && co.calccommission(sales[i]) <= 999){
                numincat[1]++;
            }
            else if (co.calccommission(sales[i]) >= 800 && co.calccommission(sales[i]) <= 899){
                numincat[2]++;
            }
            else if (co.calccommission(sales[i]) >= 700 && co.calccommission(sales[i]) <= 799){
                numincat[3]++;
            }
            else if (co.calccommission(sales[i]) >= 600 && co.calccommission(sales[i]) <= 699){
                numincat[4]++;
            }
            else if (co.calccommission(sales[i]) >= 500 && co.calccommission(sales[i]) <= 599){
                numincat[5]++;
            }
            else if (co.calccommission(sales[i]) >= 400 && co.calccommission(sales[i]) <= 499){
                numincat[6]++;
            }
            else if (co.calccommission(sales[i]) >= 300 && co.calccommission(sales[i]) <= 399){
                numincat[7]++;
            }
            else if (co.calccommission(sales[i]) >= 200 && co.calccommission(sales[i]) <= 299){
                numincat[8]++;
            }
        }
        
        //display number of sales people in each category
        System.out.println(String.format("Number of people earning commission over $1000: %s.", numincat[0]));
        System.out.println(String.format("Number of people earning commission between $900–999: %s.", numincat[1]));
        System.out.println(String.format("Number of people earning commission between $800–899: %s.", numincat[2]));
        System.out.println(String.format("Number of people earning commission between $700–799: %s.", numincat[3]));
        System.out.println(String.format("Number of people earning commission between $600–699: %s.", numincat[4]));
        System.out.println(String.format("Number of people earning commission between $500–599: %s.", numincat[5]));
        System.out.println(String.format("Number of people earning commission between $400–499: %s.", numincat[6]));
        System.out.println(String.format("Number of people earning commission between $300–399: %s.", numincat[7]));
        System.out.println(String.format("Number of people earning commission between $200–299: %s.", numincat[8]));
        
        

    }
    
}
